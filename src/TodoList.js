import React, {Component }from "react";
import TodoItem from "./TodoItem";

class TodoList extends Component {
    render() {
      return (
        <section className="main">
          <ul className="todo-list">
            {this.props.todos.map(todo => (
              <TodoItem 
              handleDeleteTodo={this.props.handleDeleteTodo(todo.id)} 
              title={todo.title} 
              completed={todo.completed}
              handleChangeCompleted={this.props.handleChangeCompleted(todo.id)}
              />
            ))}
          </ul>
        </section>
      );
    }
  }

  export default TodoList;