import React, { Component } from "react";
import "./index.css";
import todosList from "./todos.json";
import TodoList from "./TodoList";
import { Route, NavLink} from "react-router-dom";


class App extends Component {
  state = {
    todos: todosList,
    value: "",
  };

  handleChange = (event) => {
    this.setState({ value: event.target.value});
  };

  handleCreateTodo = event => {
    if(event.key === "Enter") {
      const newTodo = {
        userId: 1,
        id: Math.floor(Math.random() * 1000000),
        title: this.state.value,
        completed: false,
      };
      
      const newTodos = this.state.todos.slice();
      newTodos.push(newTodo);
      this.setState({ todos: newTodos,
          value: ""
      })
    }
  };

  handleDeleteTodo = todoIdToDelete => event => {
    const newTodos = this.state.todos.slice();
    const todoIndexToDelete = newTodos.findIndex( todo => {
      if(todo.id === todoIdToDelete) {
        return true;
      }
      else {
          return false;
      }
    });
    console.log(this.state.todos)
    newTodos.splice(todoIndexToDelete, 1);

    this.setState({todos: newTodos});
  };

  handleChangeCompleted = event => () => {
    const newTodos = this.state.todos.slice();
    newTodos.forEach( todo => {
      if(todo.id === event) {
        todo.completed = !todo.completed;
      }
    })
    this.setState({ todos: newTodos });
  };

  handleDeleteCompleted = event => {
    const newTodos = this.state.todos.filter( todo => {
      if(todo.completed === true) {
        return false;
      }
      else {
        return true;
      }

    });
    this.setState({ todos: newTodos});
    
  };
//==================================================
  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autofocus
            value={this.state.value}
            onChange={this.handleChange}
            onKeyDown={this.handleCreateTodo}
          />
        </header>
        <Route 
          exact path="/"
          render= {() => (
            <TodoList 
              handleDeleteTodo={this.handleDeleteTodo} 
              todos={this.state.todos} 
              handleChangeCompleted={this.handleChangeCompleted}
              />
            )}
        />

        <Route 
          exact path="/completed"
          render= {() => (
            <TodoList 
              handleDeleteTodo={this.handleDeleteTodo} 
              todos={this.state.todos.filter(todo => todo.completed === true )} 
              handleChangeCompleted={this.handleChangeCompleted}
              />
            )}
        />
          

        <Route 
        exact
          path="/active"
          render= {() => (
            <TodoList 
              handleDeleteTodo={this.handleDeleteTodo} 
              todos={this.state.todos.filter(todo => todo.completed !== true )} 
              handleChangeCompleted={this.handleChangeCompleted}
              />
            )}
        />


        <footer className="footer">
          <span className="todo-count">
            <strong>{this.state.todos.filter(todo => todo.completed !== true).length}</strong> item(s) left
          </span>
          <ul className="filters">
            <li>
              <NavLink exact to="/" activeClassName="selected">All</NavLink>
            </li>
            <li>
              <NavLink exact to="/active" activeClassName="selected">Active</NavLink>
            </li>
            <li>
              <NavLink exact to="/completed" activeClassName="selected">Completed</NavLink>
            </li>
          </ul>

          <button 
            className="clear-completed" 
            onClick={this.handleDeleteCompleted}>Clear completed
          </button>
          
        </footer>
      </section>
    );
  };
}

export default App;
